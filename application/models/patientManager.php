<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of patientManager
 *
 * @author benziane djazia
 */
class patientManager implements iModel
{

    private $_pdo;

    const INSRT_RQST_UTLSTR = "INSERT INTO utilisateur_tbl (nom_utlstr,prenom_utlstr,email_utlstr,telephone_utlstr,login_utlstr,password_utlstr,adresse_utlstr) "
        . "VALUES (:nom,:prenom,:email,:telephone,:login,:password,:adresse)";

    const INSRT_RQST_ptn = "INSERT INTO patient_tbl (id_ptn,dateAnniversaire_ptn,reference_ptn) "
        . "VALUES (:id,:dateAnniversaire,:reference)";

    const INSRT_RQST_ADRS = "INSERT INTO adresse_tbl (numeroVoie,libelleVoie,codePostal,ville,pays ) "
        . "VALUES (:numeroVoie,:libelleVoie,:codePostal,:ville,:pays)";

    const SLCT_RQST_UTLSTR_ADRS_BY_ID = "SELECT nom_utlstr,prenom_utlstr,email_utlstr,telephone_utlstr,login_utlstr,password_utlstr,adresse_utlstr FROM utilisateur_tbl "
        . " JOIN adresse_tbl WHERE id_utlstr = id_adresse AND id_utlstr = :id ";

    const SLCT_RQST_UTLSTR_ptn_BY_ID = "SELECT nom_utlstr,prenom_utlstr,email_utlstr,telephone_utlstr,login_utlstr,password_utlstr,adresse_utlstr FROM utilisateur_tbl "
        . " JOIN patient_tbl WHERE id_utlstr = id_ptn AND id_utlstr = :id ";

    const SLCT_RQST_UTLSTR_ptn_BY_KEYWORD = "SELECT DISTINCT nom_utlstr,prenom_utlstr,email_utlstr,telephone_utlstr,login_utlstr,password_utlstr,dateAnniversaire_ptn,reference_ptn,adresse_utlstr FROM utilisateur_tbl "
        . " JOIN patient_tbl ON id_utlstr = id_ptn AND (nom_utlstr regexp :nom OR prenom_utlstr regexp :prenom OR email_utlstr regexp :email OR telephone_utlstr regexp :telephone)";

    public function __construct(\PDO $PDO)
    {
        $this->setPdo($PDO);
    }

    public function add(object $patient, object $adresse):array
    {

        $flag = false;
        $msg = "";
        try {
            //Insertion d'un adresse
            $this->getPdo()->beginTransaction();
            $statement_adrs = $this->getPdo()->prepare(self::INSRT_RQST_ADRS);
            $statement_adrs->bindValue(":numeroVoie", $adresse->getnumeroVoie(), PDO::PARAM_STR);
            $statement_adrs->bindValue(":libelleVoie",$adresse->getlibelleVoie() , PDO::PARAM_STR);
            $statement_adrs->bindValue(":codePostal", $adresse->getcodePostal(), PDO::PARAM_STR);
            $statement_adrs->bindValue(":ville", $adresse->getville(), PDO::PARAM_STR);
            $statement_adrs->bindValue(":pays", $adresse->getpays(), PDO::PARAM_STR);
            $statement_adrs->setFetchMode(PDO::FETCH_ASSOC);
            $statement_adrs->execute();
            $id_adresse = $this->getPdo()->lastInsertId();
            //Insertion d'un utilisateur
             //Début de la transaction et sortie du mode autocommit
            $statement_utlstr = $this->getPdo()->prepare(self::INSRT_RQST_UTLSTR);
            $statement_utlstr->bindValue(":nom", $patient->getNom(), PDO::PARAM_STR);
            $statement_utlstr->bindValue(":prenom", $patient->getPrenom(), PDO::PARAM_STR);
            $statement_utlstr->bindValue(":email", $patient->getEmail(), PDO::PARAM_STR);
            $statement_utlstr->bindValue(":telephone", $patient->getTelephone(), PDO::PARAM_STR);
            $statement_utlstr->bindValue(":login", $patient->getLogin(), PDO::PARAM_STR);
            $statement_utlstr->bindValue(":adresse", $id_adresse, PDO::PARAM_INT);
             $statement_utlstr->bindValue(":password", $patient->getPassword(), PDO::PARAM_STR);
            $statement_utlstr->execute();
            $id = $this->getPdo()->lastInsertId();

            //Insertion d'un patient
            $statement_ptn = $this->getPdo()->prepare(self::INSRT_RQST_ptn);
            $statement_ptn->bindValue(":id", $id, PDO::PARAM_INT);

            $statement_ptn->bindValue(":dateAnniversaire", $patient->getDateAnniversaire(), PDO::PARAM_STR);
            $statement_ptn->bindValue(":reference", $patient->getreference(), PDO::PARAM_STR);

            $statement_ptn->execute();


            $this->getPdo()->commit(); // Validation des requêtes
        } catch (Exception $exc) {
            $flag = true;
            $msg = $exc->getTraceAsString();
            $this->getPdo()->rollBack(); // Si erreur => annulation des modifications 
        }
         return ["err_flag" => $flag, "error_msg" => $msg];
    }

    public function count($param)
    {
    }

    public function delete($params)
    {
    }

    public function exists($param)
    {
    }

    public function get(string $keyword): array
    {
        $patient = null;
        $listepatients = array();
        try {
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_UTLSTR_ptn_BY_KEYWORD);
            $statement->bindValue(":nom", $keyword, PDO::PARAM_STR);
            $statement->bindValue(":prenom", $keyword, PDO::PARAM_STR);
            $statement->bindValue(":email", $keyword, PDO::PARAM_STR);
            $statement->bindValue(":telephone", $keyword, PDO::PARAM_STR);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            while ($tupe = $statement->fetch()) {
                $dataTbTuple = $this->extractDatas($tupe);
                $patient = new patient($dataTbTuple);
                $listepatients[] = $patient;
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return $listepatients;
    }

    public function getById(int $id): patient
    {
        $patient = null;
        try {
           
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_UTLSTR_ptn_BY_ID);
            $statement->bindValue(":id", $id, PDO::PARAM_INT);
            //ou
            //$statement->execute(array(":id" => intval($id)));
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $array2 = $statement->fetch();
            $array = $this->extractDatas($array2);
            $patient = new patient($array);
            ////////////////////////////////////


            //ou
            //$statement->execute(array(":id" => intval($id)));
     
        } catch (Exception $exc) {

            echo $exc->getTraceAsString();
        }

        return $patient;
    }

    public function getById1(int $tab_utlstr ): Adresse
    {
        $adresse = null;
        try {
           
    
        

            $statement = $this->getPdo()->prepare(self::SLCT_RQST_UTLSTR_ADRS_BY_ID);
            $statement->bindValue(":adresse",$tab_utlstr["adresse_utlstr"],  PDO::PARAM_INT);
            //ou
            //$statement->execute(array(":id" => intval($id)));
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $tab_utlstr = $statement->fetch();
            $array = $this->extractDatas($tab_utlstr);
            $adresse = new Adresse($array);
        } catch (Exception $exc) {

            echo $exc->getTraceAsString();
        }

        return $adresse;
    }
    ////////////////////////////////////////
    public function getList(array $param)
    {
    }

    public function update($params)
    {
    }

    function getPdo()
    {
        return $this->_pdo;
    }

    function setPdo($pdo): void
    {
        $this->_pdo = $pdo;
    }

    function extractDatas($array): array
    {
        $datas = null;
        foreach ($array as $key => $value) {
            $datas[str_replace(array("_utlstr", "_msr", "_ptn", "_adresse"), "", $key)] = $value;
        }
        return $datas;
    }
}
