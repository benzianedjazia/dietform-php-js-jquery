<?php

/**
 * Description of adresseMnagare
 *
 * @author GRICOLAT Didier
 */
class adresseMnagare {

    private $_pdo;

    const SLCT_RQST_ADRS_EXIST = "SELECT COUNT(*) as count FROM adresse_tbl WHERE numeroVoie =:numeroVoie AND libelleVoie = :libelleVoie AND codePostal = :codePostal AND ville = :ville AND pays = :pays";
    const SLCT_RQST_ADRS_DATAS = "SELECT numeroVoie,libelleVoie,codePostal,ville,pays FROM adresse_tbl WHERE numeroVoie =:numeroVoie AND libelleVoie = :libelleVoie AND codePostal = :codePostal AND ville = :ville AND pays = :pays";

    public function __construct(\PDO $PDO) {
        $this->setPdo($PDO);
    }

    public function access($numeroVoie,$libelleVoie,$codePostal,$ville,$pays): bool {
        $rspAccess = false;
        $msg = "";
        $count = null;
        try {
            //Insertion d'un adresse
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_ADRS_EXIST);
            $statement->bindValue(":numeroVoie", $numeroVoie, PDO::PARAM_STR);
            $statement->bindValue(":libelleVoie", $libelleVoie, PDO::PARAM_STR);
            $statement->bindValue(":codePostal", $codePostal, PDO::PARAM_STR);
            $statement->bindValue(":ville", $ville, PDO::PARAM_STR);
            $statement->bindValue(":pays", $pays, PDO::PARAM_STR);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $count = intval($statement->fetch()['count']);
            if (isset($count) && $count >= 1):
                $rspAccess = true;
            endif;
        } catch (Exception $exc) {
            $msg = $exc->getTraceAsString();
        }
        return $rspAccess;
    }

    public function getDatasSession($numeroVoie,$libelleVoie,$codePostal,$ville,$pays): Adresse {
        $datasUser = null;
        $adresse = null;
        try {
            //Lecture d'un utilisateur
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_ADRS_DATAS);
            $statement->bindValue(":numeroVoie", $numeroVoie, PDO::PARAM_STR);
            $statement->bindValue(":libelleVoie", $libelleVoie, PDO::PARAM_STR);
            $statement->bindValue(":codePostal", $codePostal, PDO::PARAM_STR);
            $statement->bindValue(":ville", $ville, PDO::PARAM_STR);
            $statement->bindValue(":pays", $pays, PDO::PARAM_STR);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $adresse = new Adresse($this->extractSuffixFromDatasBase($statement->fetch()));
        } catch (Exception $exc) {
            print $exc->getTraceAsString();
        }
        return $adresse;
    }

    function getPdo() {
        return $this->_pdo;
    }

    function setPdo($pdo): void {
        $this->_pdo = $pdo;
    }
    /**
     * Fonction servant à supprimer le suffix existant sur les libellés de champs
     * @param type $array : retour de la base de données
     * @return array : dont les labelles des $keys ne présente plus de suffix.
     */
    function extractSuffixFromDatasBase($array): array {
        $datas = null;
        foreach ($array as $key => $value) {
            $datas[str_replace(array("_adresse"), "", $key)] = $value;
        }
        return $datas;
    }

}
