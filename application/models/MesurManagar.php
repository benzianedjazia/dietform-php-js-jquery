<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MesurManagerd
 *
 * @author GRICOLAT Didier
 */
class MesurManagar
{

    private $_pdo;

    const INSRT_RQST_MSR = "INSERT INTO mesur_tbl (calculePI, calculeMB, calculeEP, calculeimc,diagnostique, taille, poids, age, sexe) "
        . "VALUES (:calculePI, :calculeMB, :calculeEP, :calculeimc,:diagnostique, :taille, :poids, :age, :sexe)";
    const SLCT_RQST_MSR_BY_ID = "SELECT id_msr, calculePI, calculeMB, calculeEP, calculeimc,diagnostique, taille, poids, age, sexe  FROM mesur_tbl WHERE id_msr=:id calculePI=:calculePI AND calculeMB=:calculeMB AND calculeEP=:calculeEP AND calculeimc=:calculeimc AND diagnostique=:diagnostique AND taille=:taille AND poids=:poids AND age=:age  AND sexe=:sexe";

    public function __construct(\PDO $PDO)
    {
        $this->setPdo($PDO);
    }

    public function add(object $Mesur): array
    {
        var_dump($Mesur);
        $flag = false;
        $msg = "";
        try {
            //Insertion d'un adresse
            // $this->getPdo()->beginTransaction();
            $statement = $this->getPdo()->prepare(self::INSRT_RQST_MSR);
            $statement->bindValue(":calculePI", strval($Mesur->getcalculePI()), PDO::PARAM_STR);
            $statement->bindValue(":calculeMB", strval($Mesur->getcalculeMB()), PDO::PARAM_STR);
            $statement->bindValue(":calculeEP", strval($Mesur->getcalculeEP()), PDO::PARAM_STR);
            $statement->bindValue(":calculeimc", strval($Mesur->getcalculeimc()["imc"]), PDO::PARAM_STR);
            $statement->bindValue(":diagnostique", strval($Mesur->getcalculeimc()["diagnostique"]), PDO::PARAM_STR);
            
            $statement->bindValue(":taille", $Mesur->gettaille(), PDO::PARAM_STR);
            $statement->bindValue(":poids", $Mesur->getpoids(), PDO::PARAM_STR);
            $statement->bindValue(":age", $Mesur->getage(), PDO::PARAM_STR);
            $statement->bindValue(":sexe", $Mesur->getsexe(), PDO::PARAM_STR);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $id = $this->getPdo()->lastInsertId();


            // Validation des requêtes
        } catch (Exception $exc) {
            $flag = true;
            $msg = $exc->getTraceAsString();
            // $this->getPdo()->rollBack(); // Si erreur => annulation des modifications 
        }
        return ["err_flag" => $flag, "error_msg" => $msg];
    }

    public function count($param)
    {
    }

    public function delete($params)
    {
    }

    public function exists($param)
    {
    }

    // public function getById(int $id): Mesur
    // {
    //     $Mesur = null;
    //     try {
    //         $this->getPdo()->beginTransaction();
    //         $statement = $this->getPdo()->prepare(self::SLCT_RQST_MSR_BY_ID);
    //         $statement->bindValue(":id", $id, PDO::PARAM_INT);
    //         //   $statement->bindValue(":calculePI", $calculePI, PDO::PARAM_INT);
    //         //   $statement->bindValue(":calculeMB", $calculeMB, PDO::PARAM_INT);
    //         //   $statement->bindValue(":calculeEP", $calculeEP, PDO::PARAM_INT);
    //         //   $statement->bindValue(":calculeimc", $calculeimc, PDO::PARAM_INT);
    //         //   $statement->bindValue(":taille", $taille, PDO::PARAM_INT);
    //         //   $statement->bindValue(":poids", $poids, PDO::PARAM_INT);
    //         //   $statement->bindValue(":age", $age, PDO::PARAM_STR);
    //         //   $statement->bindValue(":sexe", $sexe, PDO::PARAM_STR);
    //         //ou
    //         $statement->execute(array(":id" => intval($id)));
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         $statement->execute();
    //         $array = $statement->fetch();
    //         $array = $this->extractDatas($array);
    //         $Mesur = new Mesur($array);



    //         //ou


    //     } catch (Exception $exc) {

    //         echo $exc->getTraceAsString();
    //     }

    //     return $Mesur;
    // }

    /////////////////////////////////////////////////////////////////////////////////////////

    // public function get(string $keyword): array
    // {
    //     $Mesur = null;
    //     $listeMesurs = array();
    //     try {
    //         $statement = $this->getPdo()->prepare(self::SLCT_RQST_UTLSTR_ptn_BY_KEYWORD);
    //         $statement->bindValue(":nom", $keyword, PDO::PARAM_STR);
    //         $statement->bindValue(":prenom", $keyword, PDO::PARAM_STR);
    //         $statement->bindValue(":email", $keyword, PDO::PARAM_STR);
    //         $statement->bindValue(":telephone", $keyword, PDO::PARAM_STR);
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         $statement->execute();
    //         while ($tupe = $statement->fetch()) {
    //             $dataTbTuple = $this->extractDatas($tupe);
    //             $Mesur = new Mesur($dataTbTuple);
    //             $listeMesurs[] = $Mesur;
    //         }
    //     } catch (Exception $exc) {
    //         echo $exc->getTraceAsString();
    //     }
    //     return $listeMesurs;
    // }

    // 

    // public function getById1(int $tab_utlstr ): Adresse
    // {
    //     $Mesur = null;
    //     try {




    //         $statement = $this->getPdo()->prepare(self::SLCT_RQST_UTLSTR_ADRS_BY_ID);
    //         $statement->bindValue(":adresse",$tab_utlstr["adresse_utlstr"],  PDO::PARAM_INT);
    //         //ou
    //         //$statement->execute(array(":id" => intval($id)));
    //         $statement->setFetchMode(PDO::FETCH_ASSOC);
    //         $statement->execute();
    //         $tab_utlstr = $statement->fetch();
    //         $array = $this->extractDatas($tab_utlstr);
    //         $Mesur = new Adresse($array);
    //     } catch (Exception $exc) {

    //         echo $exc->getTraceAsString();
    //     }

    //     return $Mesur;
    // }
    ////////////////////////////////////////
    public function getList(array $param)
    {
    }

    public function update($params)
    {
    }

    function getPdo()
    {
        return $this->_pdo;
    }

    function setPdo($pdo): void
    {
        $this->_pdo = $pdo;
    }

    function extractDatas($array): array
    {
        $datas = null;
        foreach ($array as $key => $value) {
            $datas[str_replace(array("_utlstr", "_msr", "_ptn", "_adresse"), "", $key)] = $value;
        }
        return $datas;
    }
}
