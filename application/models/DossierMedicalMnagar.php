<?php

/**
 * Description of adresseMnagare
 *
 * @author GRICOLAT Didier
 */
class DossierMedicalMnagare {

    private $_pdo;

    const SLCT_RQST_DM_EXIST = "SELECT COUNT(*) as count FROM dossiermedical_tbl WHERE antecedents_dm  =:antecedents  AND patient_dm  = :patient  ";
    const SLCT_RQST_DM_DATAS = "SELECT id_dm antecedents_dm ,patient_dm  FROM dossiermedical _tbl WHERE id_dm=:id AND antecedents_dm  =:antecedents  AND patient_dm  = :patient ";

    public function __construct(\PDO $PDO) {
        $this->setPdo($PDO);
    }

    public function add($antecedents,$patient ): bool {
        $rspAccess = false;
        $msg = "";
        $count = null;
        try {
            //Insertion 36+
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_DM_EXIST);
            $statement->bindValue(":antecedents ", $antecedents , PDO::PARAM_STR);
            $statement->bindValue(":patient ", $patient , PDO::PARAM_STR);
      
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            $statement->execute();
            $count = intval($statement->fetch()['count']);
            if (isset($count) && $count >= 1):
                $rspAccess = true;
            endif;
        } catch (Exception $exc) {
            $msg = $exc->getTraceAsString();
        }
        return $rspAccess;
    }

    public function getDatasSession($antecedents ,$patient): dossierMedical {
        $datasUser = null;
        $adresse = null;
        try {
            //Lecture d'un utilisateur
            $statement = $this->getPdo()->prepare(self::SLCT_RQST_DM_DATAS);
            $statement->bindValue(":antecedents", $antecedents, PDO::PARAM_STR);
            $statement->bindValue(":patient", $patient, PDO::PARAM_STR);
          
            $statement->execute();
            $adresse = new dossierMedical($this->extractSuffixFromDatasBase($statement->fetch()));
        } catch (Exception $exc) {
            print $exc->getTraceAsString();
        }
        return $adresse;
    }

    function getPdo() {
        return $this->_pdo;
    }

    function setPdo($pdo): void {
        $this->_pdo = $pdo;
    }
    /**
     * Fonction servant à supprimer le suffix existant sur les libellés de champs
     * @param type $array : retour de la base de données
     * @return array : dont les labelles des $keys ne présente plus de suffix.
     */
    function extractSuffixFromDatasBase($array): array {
        $datas = null;
        foreach ($array as $key => $value) {
            $datas[str_replace(array("_adresse"), "", $key)] = $value;
        }
        return $datas;
    }

}
