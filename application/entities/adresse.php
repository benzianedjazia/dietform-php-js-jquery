<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Administrateur
 *
 * @author didie
 */
class Adresse implements JsonSerializable {

    private $_numeroVoie;
    private $_libelleVoie;
    private $_codePostal;
    private $_ville;
    private $_pays;
    public function __construct(array $params) {
        $this->hydrate($params);
    }

    function hydrate($params) {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    function getnumeroVoie() {
        return $this->_numeroVoie;
    }

    function getlibelleVoie() {
        return $this->_libelleVoie;
    }

    function getcodePostal() {
        return $this->_codePostal;
    }

    function getville() {
        return $this->_ville;
    }

    function setnumeroVoie($numeroVoie): void {
        $this->_numeroVoie = $numeroVoie;
    }

    function setlibelleVoie($libelleVoie): void {
        $this->_libelleVoie = $libelleVoie;
    }

    function setcodePostal($codePostal): void {
        $this->_codePostal = $codePostal;
    }

    function setpays($pays): void {
        $this->_pays = $pays;
    }
    function setville($ville): void {
        $this->_ville = $ville;
    }
    function getpays() {
        return $this->_pays;
    }


    public function jsonSerialize() {
        return ["numeroVoie" => $this->getnumeroVoie(),
            "libelleVoie" => $this->getlibelleVoie(),
            "codePostal" => $this->getcodePostal(),
            "pays" => $this->getcodePostal()
        ];
    }

    public function getCompleteName() {
        return $this->getnumeroVoie() . "&nbsp" . $this->getlibelleVoie();
    }

}
