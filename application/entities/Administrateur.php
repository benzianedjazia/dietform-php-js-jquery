<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Administrateur
 *
 * @author didie
 */
class Administrateur implements JsonSerializable {

    private $_nom;
    private $_prenom;
    private $_identifiant;
    private $_password;

    public function __construct(array $params) {
        $this->hydrate($params);
    }

    function hydrate($params) {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    function getNom() {
        return $this->_nom;
    }

    function getPrenom() {
        return $this->_prenom;
    }

    function getIdentifiant() {
        return $this->_identifiant;
    }

    function getPassword() {
        return $this->_password;
    }

    function setNom($nom): void {
        $this->_nom = $nom;
    }

    function setPrenom($prenom): void {
        $this->_prenom = $prenom;
    }

    function setIdentifiant($identifiant): void {
        $this->_identifiant = $identifiant;
    }

    function setPassword($password): void {
        $this->_password = $password;
    }

    public function jsonSerialize() {
        return ["nom" => $this->getNom(),
            "prenom" => $this->getPrenom(),
            "identifiant" => $this->getIdentifiant()
        ];
    }

    public function getCompleteName() {
        return $this->getNom() . "&nbsp" . $this->getPrenom();
    }

}
