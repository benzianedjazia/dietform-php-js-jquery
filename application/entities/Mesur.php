<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mesur
 *
 * @author didie
 */
// namespace app\helper;
class Mesur implements JsonSerializable
{

    private $_calculePI;
    private $_calculeMB;
    private $_calculeEP;
    private $_calculeimc=array();
    private $_taille;
    private $_poids;
    private $_age;
    private $_sexe;


    public function __construct($params)
    {
        
        // $this->_taille = $taille;
        // $this->_poids = $poids;
        // $this->_age = $age;
        // $this->_sexe = $sexe;
        $this->hydrate($params);
        $this->calculePI();
        $this->calculeMB();
        $this->calculeEP();
        $this->calculeimc();
    }

    function hydrate($params)
    {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
    function getage()
    {
        return $this->_age;
    }
    function setage($age): void
    {
        $this->_age = $age;
    }
    function getcalculeimc()
    {
        return $this->_calculeimc;
    }


    function getsexe()
    {
        return $this->_sexe;
    }
    function setsexe($sexe): void
    {
        $this->_sexe = $sexe;
    }







    function settaille($taille): void
    {
        $this->_taille = $taille;
    }

    function gettaille()
    {
        return $this->_taille;
    }
    function getpoids()
    {
        return $this->_poids;
    }
    function setpoids($poids): void
    {
        $this->_poids = $poids;
    }



    function getcalculePI()
    {
        return $this->_calculePI;
    }

    function getcalculeMB()
    {
        return $this->_calculeMB;
    }

    function getcalculeEP()
    {
        return $this->_calculeEP;
    }


    function setcalculePI($calculePI): void
    {
        $this->_calculePI = $calculePI;
    }

    function setcalculeMB($calculeMB): void
    {
        $this->_calculeMB = $calculeMB;
    }

    function setcalculeEP($calculeEP): void
    {
        $this->_calculeEP = $calculeEP;
    }

    function calculeimc()
    {

        $imc = 0;
        $tabresultat = array();
        $imc = round( $this->getpoids() / pow(($this->gettaille() / 100), 2));
        $tabresultat["imc"] = $imc;
        switch ($imc) {
            case ($imc < 26):
                $tabresultat["diagnostique"] = "bonne sante";
                break;
            case ($imc < 31):
                $tabresultat["diagnostique"] = "sur poids";
                break;
            case ($imc < 33):
                $tabresultat["diagnostique"] = "obésité modérée ";
                break;
            case ($imc > 33):
                $tabresultat["diagnostique"] = "obisité morbide";
                break;
        }

       $this->setcalculeimc($tabresultat);
    }
    function setcalculeimc( $calculeimc): void
    {
        $this->_calculeimc = $calculeimc;
    }
    function calculePI()
    {
        if ($this->getsexe() == "femme") {
            $calculePI = ($this->gettaille() - 100) - (($this->gettaille() - 150) / 2.5);

            $this->setcalculePI($calculePI);
        } elseif ($this->getsexe() == "homme") {
            $calculePI = ($this->gettaille() - 100) - (($this->gettaille() - 150) / 4);
            $this->setcalculePI($calculePI);
        }
    }

    /**
     *@param : float;
     *@param : float;
     */
    function calculeEP()
    {
        $calculeEP = $this->getpoids() - $this->getcalculePI();
        $this->setcalculeEP($calculeEP);
    }
    function calculeMB()
    {
        if ($this->getsexe() == "femme") {
            $calculeMB = round( 0.963 * pow($this->getpoids(), 0.48) * pow($this->gettaille(), 0.5) * pow($this->getage(), -0.13));
            $this->setcalculeMB($calculeMB);
        } elseif ($this->getsexe() == "homme") {
            $calculeMB = round( 1.083 * pow($this->getpoids(), 0.48) * pow($this->gettaille(), 0.5) * pow($this->getage(), -0.13));
            $this->setcalculeMB($calculeMB);
        }
    }

    public function jsonSerialize()
    {
        return [
            "calculePI" => $this->getcalculePI(),
            "calculeMB" => $this->getcalculeMB(),
            "calculeEP" => $this->getcalculeEP(),
            "calculeimc" => $this->getcalculeimc(),
            "taille" => $this->gettaille(),
            "poids" => $this->getpoids(),
            "sexe" => $this->getsexe(),


        ];
    }

    public function getCompleteName()
    {
        return $this->getcalculePI() . "&nbsp" . $this->getcalculeMB() . $this->getcalculeEP() . $this->getcalculeimc() . $this->gettaille() . $this->gettaille() . $this->getpoids() . $this->getsexe();
    }
}
