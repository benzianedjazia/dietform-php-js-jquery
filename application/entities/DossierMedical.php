<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dossierMedical
 *
 * @author didie
 */
class dossierMedical  implements JsonSerializable {

    private $_antecedents;
 

    public function __construct(array $params) {
        $this->hydrate($params);
    }

    function hydrate($params) {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    function getantecedents() {
        return $this->_antecedents;
    }

  
  

    function setantecedents($antecedents): void {
        $this->_antecedents = $antecedents;
    }

  
 

    public function jsonSerialize() {
        return ["antecedents" => $this->getantecedents(),
         
        ];
    }

    public function getCompleteName() {
        return $this->getantecedents();
    }

}
