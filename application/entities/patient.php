<?php

//namespace \application\entities;

/**
 * Description of patient
 *
 * @author GRICOLAT Didier
 */
class patient extends Utilisateur implements JsonSerializable {
    private $_dateAnniversaire;
    private $_reference;
   

    public function __construct(array $params) {
     
        $this->hydrate($params);
        $this->setreference(self::referenceGenerator());
    }
 
    function hydrate($params) {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
    static function referenceGenerator(){
        $value =[];
        for ($cpt =0; $cpt<5; $cpt++){
            $values [] = strval(rand(1,9));
        }
        return implode("",$values);
    }
    function getDateAnniversaire() {
        return $this->_dateAnniversaire;
    }

    function getreference() {
        return $this->_reference;
    }

    function setreference($reference): void {
        $this->_reference = $reference;
    }

    function setDateAnniversaire($dateAnniversaire): void {
        $this->_dateAnniversaire = $dateAnniversaire;
    }
    public function jsonSerialize() {
        return ["nom" => $this->getNom(),
            "prenom" => $this->getPrenom(),
            "email" => $this->getEmail(),
            "telephone" => $this->getTelephone(),
            "reference" => $this->getreference(),
            "dateAnniversaire" => $this->getDateAnniversaire()
        ];
    }

}
