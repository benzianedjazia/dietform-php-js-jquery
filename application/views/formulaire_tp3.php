<!-- <?php
        session_start();
        ?> -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <link rel="stylesheet" href="../../asset/css/formulaire_tp3.css"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="http://www.dietform.fr">
    <link rel="stylesheet" href="../../asset/css/home.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js" integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous"></script>

    <title>Document</title>
</head>
<script>
    $("#form_inscription").submit(function(e) {
        e.preventDefault();
        inscriptionClient();

    });

    function inscriptionClient() {
        //Récupération du formulaire
        var form = $("#form_inscription").get(0);
        //Lecteur de la valeur de l'attribut action du formulaire
        var url = form.getAttribute("action");
        var formData = new FormData(form);
        ajaxOptions.data = formData;
        ajaxOptions.url = url;
        $.ajax(ajaxOptions).done(function(clbck) {
            if (clbck.err_flag) {
                alert(clbck.err_msg);
            } else {
                let flag_response = "Les donnée ont été enregistrées!";
                launchDialogInfo(flag_response);
                reset_form();
            }
            dis_panel_flw();
        }).fail(function(e) {
            console.log(e);
            alert("Error!");
        }).always(function() {
            //dis_panel_flw();
        });
    }
    $(document).ready(function() {


        $(".nav-link").click(function() {
            $(".nav-link").removeClass("active");
            $(this).addClass("active");
        })




    });
</script>
<style>
    .active {
        background-color: #bd0dfd !important;
    }


    #ss_ctn_from legend {
        text-align: center;
        text-transform: uppercase;
    }

    /**/
    .form-control:focus {
        box-shadow: none !important;
        border-color: silver;
    }

    #submit {
        padding: 20px 50px;
        font-weight: bold;
        color: rgb(0, 0, 0);
        background: linear-gradient(0.25turn, #017a30, #ebf8e1, #017a30);
        font-size: 20px;
        font-style: italic;
    }

    #footer {

        background: linear-gradient(rgb(255, 255, 255, 1), rgb(0, 112, 37));
        padding: 10px;
        text-align: center;
        left: 0;
        right: 0;
        top: 95%;
        font-weight: bolder;
    }

    div {
        font-size: 25px;
        font-style: italic;
        text-align: left;
    }

    input {
        padding: 10px;
        margin: 5px;
        size: 100px;
        border-radius: 10px;
        box-shadow: -2px 2px 2px 2px rgb(83, 79, 79);
        margin-right: 10px;
        list-style-position: right;
    }

    .div12 {
        text-align: center;
    }

    .mt-4 {
        margin: 25px;
        text-align: center;
    }

    h1 {
        text-align: center;
        font-style: italic;
        font-size: 20px;
        border: 3px solid;
        margin: 5px;
        padding: -20px;
        border-radius: 20px;
        box-shadow: -10px 8px 8px 8px rgb(110, 107, 107);
        background-color: rgb(255, 255, 255);
        color: rgb(2, 65, 31);
    }

    #footer {
        position: absolute;
        background: linear-gradient(rgb(255, 255, 255, 1), rgb(0, 112, 37));
        padding: 10px;

        left: 0;
        right: 0;
        top: 93%;
        font-weight: bolder;
    }

    #block {

        width: 1200px;
        display: table;
        position: absolute;
        margin-top: 100px;

        border: solid 3px;
        font-size: 20px;
        padding: 10px;
        bottom: 0;
        text-align: center;
        top: 10%;
        left: 50%;
        right: 50%;
        transform: translate(-50%, -10%);
        border-radius: 80px;
    }

    #block1 {
        width: 50%;
        display: table-cell;
        padding: 10px;
        margin: 5px;
    }

    #block2 {
        width: 50%;
        display: table-cell;
        padding: 10px;
        margin: 5px;

    }

    .ajuste_label_input {
        display: inline-block;
        width: 150px;

    }

    .ajuste_label_input2 {
        width: 200px;
        display: inline-block;

    }
</style>

<body>


    <body>
        <main id="compte">

            <div id="h1">
                <a class="navbar-brand">
                    <img src="../../asset/images/art1.jpg" class="bi me-2" style="border:2px solid ; border-radius:50%" width="60" height="60">
                </a>
                MEILLEURE-SANTE.FR
            </div>
            <div id="nav" class="navbar  navbar-expand-md">
                <button type="button" class="navbar-toggler mx-3" data-bs-toggle="collapse" data-bs-target="#content_nav">
                    <span class="navbar-toggler-icon">
                        <i class="bi bi-blockquote-left" style=" font-size:40px"></i>
                    </span>
                </button>
                <nav id="content_nav" class=" collapse navbar-collapse">
                    <ul class="navbar-nav nav-pills" style="float: right;">
                        <li class="nav-item"><a class="nav-link fs-5 active" href="#">Accueil</a> </li>
                        <li class="nav-item"><a class="nav-link fs-5 fs-5 " href="#">Pourquoi consulter?</a> </li>
                        <li class="nav-item"><a class="nav-link fs-5 " href="#">Pour qui</a> </li>
                        <li class="nav-item"><a class="nav-link fs-5 " href="#">Honoraires</a> </li>
                        <a href="http://www.dietform.fr/application/views/formulaire_tp3.php" class="nav-link " class="nav-item"><input class="btn bg-light" type="submit" href="../controller/initialisation_tp3.php" value="Inscription" size="30" required></a>

                        <a href="http://www.dietform.fr/application/views/authentification.php" class="nav-link "> <input class="btn  bg-light  " class="nav-item" type="submit" value="connexion" size="7" required></a>

                    </ul>

                </nav>
            </div>


        </main>

        </div>


        <!-- Page Header End -->
        <section data-sctn-id="3" id="sctn_tab_3">
            <form id="form_inscription" action="http://www.dietform.fr/application/controllers/inscription.php" method="post" enctype="application/x-www-form-urlencoded">
                <div id="block" id="ss_ctn_from">
                    <input type="hidden" name="action" value="inscription">
                    <article id="block1">
                        <div>
                            <h1>information personnelle</h1>

                            <div class="mb-2">
                                <label patient for="nom_utlstr">Civilité</label>

                                <select class="form-select" id="slct_cvlt">
                                    <option selected="selected">--- Choisir ---</option>
                                    <option value="masculin">Mr</option>
                                    <option value="feminin">Mme</option>
                                </select>
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="nom_utlstr">Nom</label>
                                <input class="ajuste_label_input2" type="text" id="nom_utlstr" name="nom_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="prenom_utlstr">Prénom</label>
                                <input class="ajuste_label_input2" type="text" id="prenom_utlstr" name="prenom_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="email_utlstr">Email</label>
                                <input class="ajuste_label_input2" type="text" id="email_utlstr" name="email_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="telephone_utlstr">Téléphone</label>
                                <input class="ajuste_label_input2" type="text" id="telephone_utlstr" name="telephone_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="dateAnniversaire_utlstr">Date anniversaire</label>
                                <input class="ajuste_label_input2" type="date" id="dateAnniversaire_utlstr" name="dateAnniversaire_utlstr">
                            </div>


                            

                        </div>
                        <div class="mb-2">
                            <label class="ajuste_label_input2" class="form-label">Password</label>
                            <input class="ajuste_label_input2" class="form-control" type="password" name="password_utlstr">
                        </div>
                    </article>
                    <article id="block2">
                        <h1>Données patient</h1>
                        <div class="mb-2">
                                <label class="ajuste_label_input2" for="numeroVoie_utlstr">numeroVoie</label>
                                <input class="ajuste_label_input2" type="text" id="numeroVoie_utlstr" name="numeroVoie">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="codePostal_utlstr">codePostal</label>
                                <input class="ajuste_label_input2" type="text" id="codePostal_utlstr" name="codePostal_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="libelleVoie_utlstr">libelleVoie</label>
                                <input class="ajuste_label_input2" type="text" id="libelleVoie_utlstr" name="libelleVoie_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="ville_utlstr">ville</label>
                                <input class="ajuste_label_input2" type="text" id="ville_utlstr" name="ville_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="ajuste_label_input2" for="pays_utlstr">pays</label>
                                <input class="ajuste_label_input2" type="text" id="pays_utlstr" name="pays_utlstr">
                            </div>
                        <!-- <div class="div45">
                            <label class="ajuste_label_input2" for="Mon âge">Mon âge ans</label>
                            <span class="ajuste_label_input2"><input type="number" name="age" class="ajuste_label_input2" placeholder="ex: 30" maxlength="2" size="30" required placeholder="Indiquer votre age"></span>



                        </div>
                        <div>
                            <label class="ajuste_label_input2" for="Mon poid">Mon poids </label>
                            <span class="ajuste_label_input2"><input type="number" name="poid" class="ajuste_label_input2" placeholder="Indiquer votre poids en KG" min="0" required></span>
                        </div>
                        <div class="div45">
                            <label class="ajuste_label_input2" for="Ma taille">Ma taille </label>

                            <span class="ajuste_label_input2"> <input type="number" name="taille" class="ajuste_label_input2" placeholder="Indiquer votre taille en CM" min="0" required></span>



                        </div>
                        <div class="mt-4" > -->
                        <input type="submit" id="submit" value="S'inscrire">
                </div>
                </article>
                </div>

            </form>


        </section>
        <!-- <footer id="footer">
            <div class="div12">
            MEILLEURE-SANTE@copyright
            </div>
        </footer>   -->
    </body>

</html>