<?php 
session_start();
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="../../asset/css/page-formulaireTP2.css">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Document</title>
</head>

<body>

   
    <section id="block1">
        <div>
            <h1>Découvrez</h1>
            <h2> si vous êtes en surpoids/obésité</h2>
        </div>


    </section>


    <main>
    <?php if (isset($_SESSION["msg_erreur"])) { ?>

<?php  echo $_SESSION["msg_erreur"]; ?>
<?php }  ?>

        <form action="../controller/traitement.php" method="post">
            <section>

                <div id="block2">
                    <div>Je suis :</div>

                    <div class="divgender">
                        <input type="radio" name="sexe" value="femme" require>
                        <label for="femme">
                            Femme
                        </label>
                    </div>

                    <div class="divgender">
                        <input type="radio" name="sexe" value="homme" require>
                        <label for="homme">
                            Homme
                        </label>
                    </div>

                </div>


            </section>
            <section id="block3">


                <div class="div45">
                    <label for="Mon âge">Mon âge ans</label>
                    <br> <input type="text" name="age" placeholder="ex: 30" maxlength="2" size="30" require placeholder="Indiquer votre age" min="150">



                </div>

                <div class="div45">
                    <label for="Mon poid">Mon poids Kg</label>
                    <br> <input type="number" name="poid" placeholder="Indiquer votre poids" min="0" size="30" require max="250">



                </div>
                <div class="div45">
                    <label for="Ma taille">Ma taille CM</label>
                    <br> <input type="number" name="taille" placeholder="Indiquer votre taille en cm" size="30" min="0" require max="300">



                </div>




            </section>


            <div id="divimc">
                <input type="submit" value="valider" size="30" require>
            </div>


        </form>
        
        <?php if(isset($_SESSION["datapatient"])) { ?>
            
            <section id="resultat">
                <h2>Resultat du traitement</h2>

                <div>
                <p>votr imc  
                        <?php  echo $_SESSION["datapatient"]["dataimc"]["imc"]; ?>
                    
                    </p>
                    <p>votr imc  
                        <?php  echo $_SESSION["datapatient"]["dataimc"]["text"]; ?>
                    
                    </p>
                    
                    <p>votre MB métabolisme de base aaa
                       <?php  echo $_SESSION["datapatient"]["MB"]; ?>
                    </p>
                    <p>le poids idéal calculé
                      <?php echo  $_SESSION["datapatient"]["PI"]; ?>
                    </p>
                    <div id="div_reset"> <button id="div_reset"><a href="../controller/initialisation_form.php"> initialtation</a></button></div>
                   
                </div>
              
            </section>
        <?php } ?> 
      
    </main>
   
</body>

</html>
