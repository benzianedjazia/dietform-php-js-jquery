<?php
session_start();
/*
 * Chargement de l'autoloader afin de disposer
 */
require_once '../libraries/autoloader_.php';

if (isset($_SESSION['access_ctrl']) && $_SESSION['access_ctrl'] == hash("sha256", session_id() . "913746"))

?>

<!DOCTYPE html>
<html lang="en">
<style>
    .ajuste_label_input2 {
        width: 200px;
        display: inline-block;

    }

    h1 {
        text-align: center;
        font-style: italic;
        font-size: 20px;
        border: 3px solid;
        transform: translateY(80px);
        border-radius: 20px;
        box-shadow: -10px 8px 8px 8px rgb(110, 107, 107);
        background-color: rgb(255, 255, 255);
        color: rgb(2, 65, 31);
    }

    #footer {
        transform: translateY(610px);
        background: linear-gradient(rgb(255, 255, 255, 1), rgb(0, 112, 37));
        padding: 10px;

        left: 0;
        right: 0;
        top: 95%;
        font-weight: bolder;
    }

    #resultat h1 {


        transform: translateY(-8px);
    }

    #resultat {
        margin-left: 750px;

        width: 850px;
        text-align: center;
        bottom: 0;

        /* background-image: linear-gradient(  rgba(255, 255, 255, 0.5),rgba(206, 11, 11,1) 100%); */


        transform: translate(-50%,150%);
        top: 40%;

    }

    #block {

        width: 1200px;
        display: table;
        position: absolute;
        margin-top: 100px;

        border: solid 3px;
        font-size: 20px;
        padding: 10px;
        bottom: 0;
        text-align: center;
        top: 30%;
        left: 50%;
        right: 50%;
        transform: translate(-50%, -30%);
        border-radius: 80px;
    }
</style>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"> </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../asset/css/home.css">
    <link rel="stylesheet" href="../../asset/css/bootstrap.min.css" type="text/css">
    <script src="../../asset/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500&family=Lora:wght@600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">


    <title>MEILLEURE-SANTE.FR</title>
</head>
<script defer type="text/javascript">
    $("#form_inscription").submit(function(e) {
        e.preventDefault();
        inscriptionClient();

    });

    function inscriptionClient() {
        //Récupération du formulaire
        var form = $("#form_inscription").get(0);
        //Lecteur de la valeur de l'attribut action du formulaire
       
        var url = form.getAttribute("../controllers/traitement.php");
        var formData = new FormData(form);
        ajaxOptions.data = formData;
        ajaxOptions.url = url;
        $.ajax(ajaxOptions).done(function(clbck) {
            if (clbck.err_flag) {
                alert(clbck.err_msg);
            } else {
                let flag_response = "Les donnée ont été enregistrées!";
                launchDialogInfo(flag_response);
                reset_form();
            }
            dis_panel_flw();
        }).fail(function(e) {
            console.log(e);
            alert("Error!");
        }).always(function() {
            //dis_panel_flw();
        });
    }
    $(document).ready(function() {


        $(".nav-link").click(function() {
            $(".nav-link").removeClass("active");
            $(this).addClass("active");
        })



                /*===================================================
                 *    Logout  : Déconnexion de l'application
                 ===================================================*/
                 $("li[id='btn_actn_logout'] ").click(function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "http://www.dietform.fr/application/controllers/compte.php",
                        method: "POST",
                        data: {
                            "action": "logout"
                        }
                    }).done(function(flag_rsp) {
                        window.location.replace("http://www.dietform.fr");
                    }).fail(function() {
                        launchDialogInfo("Une erreur est survenue lors de la requête de deconnexion!");
                    }).always(function() {

                    });
                });

    });
</script>

<body>
    <main id="compte" style="z-index: 1200;">

        <div>
            <a class="navbar-brand">
                <img src="../../asset/images/art1.jpg" class="bi me-2" style="border:2px solid ; border-radius:50%" width="60" height="60">
            </a>
            MEILLEURE-SANTE.FR
        </div>
        <div id="nav" class="navbar  navbar-expand-md">
            <button type="button" class="navbar-toggler mx-3" data-bs-toggle="collapse" data-bs-target="#content_nav">
                <span class="navbar-toggler-icon">
                    <i class="bi bi-blockquote-left" style=" font-size:40px"></i>
                </span>
            </button>
            <nav id="content_nav" class=" collapse navbar-collapse">
                <ul class="navbar-nav nav-pills" style="float: right;">
                    <li class="nav-item"><a class="nav-link fs-5 active" href="http://www.dietform.fr/application/views/home.php">Accueil</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 fs-5 " href="http://www.dietform.fr/application/views/home2.php">About</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="calculeimc.php">calcule mesur</a> </li>

                    <li class="nav-item"><a class="nav-link fs-5 " href="blog.html">blog</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="testimonial.html">avis</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="contact.html">contacte</a> </li>
                    <li id="btn_actn_logout"><a class="dropdown-item" href="#">Déconnexion</a></li>

                </ul>

            </nav>
        </div>


    </main>


    <section id="block1">
        <div>

            <h1> Découvrez si vous êtes en surpoids/obésité</h1>
        </div>


    </section>



    <?php if (isset($_SESSION["msg_erreur"])) { ?>

        <?php echo $_SESSION["msg_erreur"]; ?>
    <?php }  ?>

    <form id="form_inscription" action="http://www.dietform.fr/application/controllers/Mesur.php" method="post" enctype="application/x-www-form-urlencoded">
        <input type="hidden" name="action" value="inscription">
        <main id="block">
            <section>

                <div>
                    <div>Je suis :</div>

                    <div class="divgender">
                        <input type="radio" id="sexe" name="sexe" value="femme" require>
                        <label for="femme">
                            Femme
                        </label>
                    </div>

                    <div class="divgender">
                        <input type="radio" id="sexe" name="sexe" value="homme" require>
                        <label for="homme">
                            Homme
                        </label>
                    </div>

                </div>


            </section>
            <section>


                <div class="div45">
                    <label class="ajuste_label_input2" for="Mon âge">Mon âge ans</label>
                    <br> <input class="ajuste_label_input2" type="text" id="age" name="age" placeholder="ex: 30" maxlength="2" size="30" require placeholder="Indiquer votre age" min="150">



                </div>

                <div class="div45">
                    <label class="ajuste_label_input2" for="Mon poids">Mon poids Kg</label>
                    <br> <input class="ajuste_label_input2" type="number" name="poids" id="poids" placeholder="Indiquer votre poidss" min="0" size="30" require max="250">



                </div>
                <div class="div45">
                    <label class="ajuste_label_input2" for="Ma taille">Ma taille CM</label>
                    <br> <input class="ajuste_label_input2" type="number" name="taille" id="taille" placeholder="Indiquer votre taille en cm" size="30" min="0" require max="300">



                </div>




            </section>


            <div id="divimc">
                <input class="ajuste_label_input2"  type="submit" value="valider" size="30" require>
            </div>
        </main>
        <section id="resultat">
             <?php if (isset($_SESSION["bilan"])) { ?>


                <h1>Resultat du traitement</h1>

                <div>
                    <p>votr imc
                        <input type="number" name="calculeimc" id="calculeimc" value="<?php echo $_SESSION["bilan"]["dataimc"]["imc"]; ?>">

                    </p>
                    <p>votr imc
                        <?php echo $_SESSION["bilan"]["dataimc"]["diagnostique"]; ?>

                    </p>

                    <p>votre MB métabolisme de base
                        <input type="number" name="calculeMB" id="calculeMB" value="<?php echo $_SESSION["bilan"]["MB"]; ?>">


                    </p>
                    <p>votre EP
                        <input type="number" name="calculeEP" id="calculeEP" value="<?php echo $_SESSION["bilan"]["EP"]; ?>">

                    </p>
                    <p>le poidss idéal calculé
                        <input type="number" name="calculePI" id="calculePI" value="<?php echo  $_SESSION["bilan"]["PI"]; ?>">

                    </p>
                    <div id="div_reset"> <button class="ajuste_label_input2" id="div_reset"><a href="../controllers/initialisation_form.php"> initialtation</a></button></div>

                </div>


            <?php } ?>
        </section>
    </form>




    <!-- <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a> -->

    <!-- Back to Top -->

    <!-- <section id="sct_footer">
        <footer id="footer">

            <h2> MEILLEURE-SANTE@copyright</h2>

        </footer>
    </section> -->




    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../../asset/lib/wow/wow.min.js"></script>
    <script src="../../asset/lib/easing/easing.min.js"></script>
    <script src="../../asset/lib/waypoints/waypoints.min.js"></script>
    <script src="../../asset/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Template Javascript -->
    <script src="../../asset/js/main.js"></script>
</body>

</html>