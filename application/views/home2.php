<?php
session_start();
/*
 * Chargement de l'autoloader afin de disposer
 */
require_once '../libraries/autoloader_.php';

if (isset($_SESSION['access_ctrl']) && $_SESSION['access_ctrl'] == hash("sha256", session_id() . "913746"))
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"> </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../asset/css/home.css">
    <link rel="stylesheet" href="../../asset/css/bootstrap.min.css" type="text/css">
    <script src="../../asset/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500&family=Lora:wght@600;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="lib/animate/animate.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">


    <title>MEILLEURE-SANTE.FR</title>
</head>
<script defer type="text/javascript">
    $(document).ready(function() {


        $(".nav-link").click(function() {
            $(".nav-link").removeClass("active");
            $(this).addClass("active");
        })




    });

</script>
<body>
    <main id="compte" style="z-index: 1200;">

        <div id="h1">
           <a class="navbar-brand">
                <img src="../../asset/images/art1.jpg" class="bi me-2" style="border:2px solid ; border-radius:50%" width="60" height="60">
            </a>
            MEILLEURE-SANTE.FR
        </div>
        <div id="nav"class="navbar  navbar-expand-md">
            <button  type="button" class="navbar-toggler mx-3" data-bs-toggle="collapse" data-bs-target="#content_nav">
                <span class="navbar-toggler-icon" >
                    <i class="bi bi-blockquote-left" style=" font-size:40px"></i>
                </span>
            </button>
            <nav id="content_nav" class=" collapse navbar-collapse">
                <ul class="navbar-nav nav-pills" style="float: right;">
                    <li class="nav-item"><a class="nav-link fs-5 " href="http://www.dietform.fr/application/views/home.php">Accueil</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 fs-5 active " href="http://www.dietform.fr/application/views/home2.php">About</a> </li> 
                    <li class="nav-item"><a class="nav-link fs-5 " href="calculeimc.php">calcule mesur</a> </li>

                    <li class="nav-item"><a class="nav-link fs-5 " href="blog.html">blog</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="testimonial.html">avis</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="contact.html">contacte</a> </li>

                    <a href="http://www.dietform.fr/application/views/home.php" class="nav-link "> <input class="btn  bg-light  " class="nav-item" id="submit" type="submit" value="déconnexion" size="7" required></a>

                </ul>

            </nav>
        </div>


    </main>
     <!-- Page Header Start -->
     <div class="container-fluid page-header mb-5 wow fadeIn" data-wow-delay="0.1s">
        <div class="container">
            <h1 class="display-3 mb-3 animated slideInDown">À propos de nous</h1>
            <nav aria-label="breadcrumb animated slideInDown">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a class="text-body" href="#">Home</a></li>
                    <li class="breadcrumb-item"><a class="text-body" href="#">Pages</a></li>
                    <li class="breadcrumb-item text-dark active" aria-current="page">À propos de nous</li>
                </ol>
            </nav>
        </div>
    </div>
    <!-- Page Header End -->


    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
            <div class="row g-5 align-items-center">
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.1s">
                    <div class="about-img position-relative overflow-hidden p-5 pe-0">
                        <img class="img-fluid w-100" src="../../asset/img/about.jpg">
                    </div>
                </div>
                <div class="col-lg-6 wow fadeIn" data-wow-delay="0.5s">
                    <h1 class="display-5 mb-4">Meilleurs fruits et légumes biologiques</h1>
                    <p class="mb-4">Au fil des ans, il a été subventionné à RVing. Diam est la douleur. Certaines personnes comptent sur l'appareil photo pour les aimer. Clita était la même et le véhicule et s'asseoir, mais laissez le véhicule s'asseoir RVing Two vient d'avoir une grande douleur</p>
                    <p><i class="fa fa-check text-primary me-3"></i>Au fil des ans, il a été subventionné à clita</p>





                    <p><i class="fa fa-check text-primary me-3"></i>Certaines arrière-cour aiment investir</p>
                    <p><i class="fa fa-check text-primary me-3"></i>Le duo Clita vient d'avoir une grande douleur</p>
                    <a class="btn btn-primary rounded-pill py-3 px-5 mt-3" href="">Read More</a>
                </div>
            </div>
        </div>
    </div>
    <!-- About End -->


    <!-- Firm Visit Start -->
    <div class="container-fluid bg-primary bg-icon mt-5 py-6">
        <div class="container">
            <div class="row g-5 align-items-center">
                <div class="col-md-7 wow fadeIn" data-wow-delay="0.1s">


                    <h1 class="display-5 text-white mb-3">Visitez notre cabinet</h1>
                    <p class="text-white mb-3">Au fil des ans, il a été subventionné à RVing mais laissez leDiam est la douleur. Certaines personnes comptent sur l'appareil photo et les aiment.</p>
                </div>
                <div class="col-md-5 text-md-end wow fadeIn" data-wow-delay="0.5s">
                    <a class="btn btn-lg btn-secondary rounded-pill py-3 px-5" href="">Visit Now</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Firm Visit End -->


    <!-- Feature Start -->
    <div class="container-fluid bg-light bg-icon py-6">
        <div class="container">
            <div class="section-header text-center mx-auto mb-5 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
                <h1>Les 3 garanties MEILLEURE-SANTE :</h1>


                <p>On s'occupe de vous</p>
            </div>
            <div class="row g-4">
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="bg-white text-center h-100 p-4 p-xl-5">
                        <img class="img-fluid mb-4" src="../../asset/img/da1.svg" alt="">
                        <h4 class="mb-3">4 repas équilibrés par jour</h4>
                        <p class="mb-4">Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.</p>
                        <a class="btn btn-outline-primary border-2 py-2 px-4 rounded-pill" href="">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="bg-white text-center h-100 p-4 p-xl-5">
                        <img class="img-fluid mb-4" src="../../asset/img/da2.svg" alt="">
                        <h4 class="mb-3">1 diététicien à votre écoute</h4>
                        <p class="mb-4">Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.</p>
                        <a class="btn btn-outline-primary border-2 py-2 px-4 rounded-pill" href="">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="bg-white text-center h-100 p-4 p-xl-5">
                        <img class="img-fluid mb-4" src="../../asset/img/da3.svg" alt="">
                        <h4 class="mb-3">1 appli pour suivre vos progrès</h4>
                        <p class="mb-4">Tempor ut dolore lorem kasd vero ipsum sit eirmod sit. Ipsum diam justo sed vero dolor duo.</p>
                        <a class="btn btn-outline-primary border-2 py-2 px-4 rounded-pill" href="">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Feature End -->




    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a>


    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/wow/wow.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
<!--
        <section id="sct_footer"><footer id="footer">

            <h2> MEILLEURE-SANTE@copyright</h2>

        </footer></section> -->



</body>

</html>
