<?php
session_start();
/*
 * Chargement de l'autoloader afin de disposer
 */
require_once '../libraries/autoloader_.php';

if (isset($_SESSION['access_ctrl']) && $_SESSION['access_ctrl'] == hash("sha256", session_id() . "913746"))
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"> </script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../asset/css/home.css">
    <link rel="stylesheet" href="../../asset/css/bootstrap.min.css" type="text/css">
    <script src="../../asset/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">


    <title>MEILLEURE-SANTE.FR</title>
</head>

<script defer type="text/javascript">
    $(document).ready(function() {


        $(".nav-link").click(function() {
            $(".nav-link").removeClass("active");
            $(this).addClass("active");
        })




    });
</script>

<body>
    <main id="compte">

        <div id="h1">
            <a class="navbar-brand">
                <img src="../../asset/images/art1.jpg" class="bi me-2" style="border:2px solid ; border-radius:50%" width="60" height="60">
            </a>
            MEILLEURE-SANTE.FR
        </div>
        <div id="nav" class="navbar  navbar-expand-md">
            <button type="button" class="navbar-toggler mx-3" data-bs-toggle="collapse" data-bs-target="#content_nav">
                <span class="navbar-toggler-icon">
                    <i class="bi bi-blockquote-left" style=" font-size:40px"></i>
                </span>
            </button>
            <nav id="content_nav" class=" collapse navbar-collapse">
                <ul class="navbar-nav nav-pills" style="float: right;">
                    <li class="nav-item"><a class="nav-link fs-5 active" href="#">Accueil</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 fs-5 " href="#">Pourquoi consulter?</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="#">Pour qui</a> </li>
                    <li class="nav-item"><a class="nav-link fs-5 " href="#">Honoraires</a> </li>
                    <a href="http://www.dietform.fr/application/views/formulaire_tp3.php" class="nav-link " class="nav-item"><input class="btn bg-light" id="submit" type="submit" href="../controller/initialisation_tp3.php" value="Inscription" size="30" required></a>

                    <a href="http://www.dietform.fr/application/views/authentification.php" class="nav-link "> <input class="btn  bg-light  " class="nav-item" id="submit" type="submit" value="connexion" size="7" required></a>

                </ul>

            </nav>
        </div>


    </main>
    <main>
        <div id="img4">
            <img id="img7" src="../../asset/images/dietetique-5.jpg" alt="">
        </div>
    </main>
    <main id="main">
        <section id="block1">
            <div>
                <h3>carte de localisation</h3>
                <div id="cart">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d92456.81423890933!2d1.3628029076665615!3d43.60080618478286!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebb6fec7552ff%3A0x406f69c2f411030!2sToulouse%2C%20France!5e0!3m2!1sfr!2sus!4v1646234768251!5m2!1sfr!2sus" style="border:0; width: 500px;" allowfullscreen="" loading="lazy"></iframe>

                </div>
            </div>
            <div>
                <table>


                    <h3>Honoraires</h3>

                    <tr>
                        <td>lundi </td>
                        <td>9h-18h</td>

                    </tr>
                    <tr>
                        <td>Mardi </td>
                        <td>9h-18h</td>
                    </tr>
                    <tr>
                        <td>Mercredi </td>
                        <td>9h-18h</td>
                    </tr>
                    <tr>
                        <td>Jeudi </td>
                        <td>9h-18h</td>
                    </tr>
                    <tr>
                        <td>Vendredi </td>
                        <td>9h-18h</td>
                    </tr>
                    <tr>
                        <td>Samdi </td>
                        <td>9h-16h</td>
                    </tr>
                </table>



            </div>


        </section>
        <section id="block2">
            <div>
                <p id="image"> <img src="../../asset/images/diet.jpg" alt=""> Lorem Ipsum is simply dummy text of the
                    printing and typesetting industry. Lorem
                    Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took
                    a galley of type and scrambled it to make a type specimen book. It has survived not only five
                    centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
                    popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and
                    more recently with desktop publishing software like Aldus PageMaker including versions of Lorem
                    Ipsum. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem
                    Ipsum leur faux texte par défaut, et une recherche pour 'Lorem Ipsum' vous conduira vers de nombreux
                    sites qui n'en sont encore qu'à leur phase de construction. Plusieurs versions sont apparues avec le
                    temps, parfois par accident, souvent intentionnellement (histoire d'y rajouter de petits clins
                    d'oeil, voire des phrases embarassantes).</p>
            </div>

            <!-- <div id="vd"> <iframe width="800" height="250" src="https://www.youtube.com/embed/lNTs_Wud4oA"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe></div> -->

        </section>


    </main>
    <main id="main2">

        <article id="asid1">

            <aside>

                <div>
                    <figure><img id="fig" src="../../asset/images/art1.jpg" alt=""></figure>
                </div>
                <div>
                    <h6>Comment être en bonne santé</h6>
                    <p>Un jour nous nous retournons et contemplons étonné l’édifice de notre vie, composé de toutes ces
                        briques, de tous ces jours que nous avons accumulésEn s’occupant de notre notre corps et notre
                        esprit au quotidien, nous posons tous les jours une brique dans notre édifice qui devient une
                        vie longue qui rayonne de santé. Voici dix de ses habitudes qui sont autant de briques à poser
                        chaque jour. Suivez le guide de longévité.....</p>
                </div>

            </aside>
        </article>
        <article id="asid2">
            <aside>
                <div>
                    <figure><img id="fig" src="../../asset/images/art2.jpg" alt=""></figure>
                </div>
                <h6>10 astuces pour être en bonne santé</h6>

                <p>
                    <br> 1.Limitez la viande rouge
                    <br>2. Consommez les bonnes graisses
                    <br>3. Bien mastiquer votre nourriture
                    <br>4. Mangez des fruits et légumes tous les jours
                    <br>5.Buvez du thé


                </p>

                </details>
            </aside>
        </article>
        <article id="asid3">
            <aside>
                <div>
                    <figure><img id="fig" src="../../asset/images/fitnes.jpg" alt=""></figure>
                </div>
                <p> Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant
                    impression.
                    Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur
                    anonyme,assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a
                    pas fait



        </article>

    </main>


    <main id="lire1"> <a href="http://www.dietform.fr/application/views/authentification.php"><button id="lire">découvrir nos articles</button></a> </main>

    <button id="lire" style="transform:translateY(-60px) ;" type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@mdo">calculer vos mesure</button>
    <main> <?php if (isset($_SESSION["datapatient"])) { ?>

            <section id="resultat">
                <h2>Resultat du traitement</h2>

                <div id="rslt">
                    <span class="div45">
                        <p>votr imc
                            <?php echo $_SESSION["datapatient"]["dataimc"]["imc"]; ?>

                        </p>
                    </span>
                    <span class="div45">
                        <p>votr imc
                            <?php echo $_SESSION["datapatient"]["dataimc"]["text"]; ?>

                        </p>
                    </span>
                    <span class="div45">
                        <p>votre EP
                            <?php echo $_SESSION["datapatient"]["EP"]; ?>
                        </p>
                    </span>
                    <span class="div45">
                        <p>votre MB métabolisme de base
                            <?php echo $_SESSION["datapatient"]["MB"]; ?>
                        </p>
                    </span>
                    <span class="div45">
                        <p>le poids idéal calculé
                        <?php echo  $_SESSION["datapatient"]["PI"]; ?>
                        </p>
                    </span>

                </div>
                <div id="div_reset"> <button id="div_reset"><a href="http://www.dietform.fr/application/controllers/initialisation_form1.php"> initialtation</a></button></div>

            </section>
        <?php } ?>
    </main>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Découvrez si vous êtes en surpoids/obésité</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <?php if (isset($_SESSION["msg_erreur"])) { ?>

                        <?php echo $_SESSION["msg_erreur"]; ?>
                    <?php }  ?>

                    <form action="http://www.dietform.fr/application/controllers/traitement.php" enctype="application/x-www-form-urlencoded" method="post">
                        <section>

                            <div id="block2">
                                <div>Je suis :</div>

                                <div class="divgender">
                                    <input type="radio" name="sexe" value="femme" require>
                                    <label for="femme">
                                        Femme
                                    </label>
                                </div>

                                <div class="divgender">
                                    <input type="radio" name="sexe" value="homme" require>
                                    <label for="homme">
                                        Homme
                                    </label>
                                </div>

                            </div>


                        </section>
                        <section id="block3">


                            <div class="div45">
                                <label for="Mon âge">Mon âge ans</label>
                                <br> <input type="text" name="age" placeholder="ex: 30" maxlength="2" size="30" require placeholder="Indiquer votre age" min="150">



                            </div>

                            <div class="div45">
                                <label for="Mon poid">Mon poids Kg</label>
                                <br> <input type="number" name="poids" placeholder="Indiquer votre poids" min="0" size="30" require max="250">



                            </div>
                            <div class="div45">
                                <label for="Ma taille">Ma taille CM</label>
                                <br> <input type="number" name="taille" placeholder="Indiquer votre taille en cm" size="30" min="0" require max="300">



                            </div>




                        </section>


                        <div id="divimc">
                            <input type="submit" value="valider" size="30" require>

                        </div>


                    </form>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>

    <main>

        <div>
            <h3> Contact et rendez-vous</h3>
            <form id="form" action="">

                <section id="formsec1">
                    <aside id="form1">
                        <h4> <i id="icon" class="fa fa-envelope" style="color: #017a30; font-size:40px"> </i>Coordonnées
                        </h4>
                        <div>
                            <h5><i id="icon" class="fa fa-map-marker" style="color: #017a30; font-size:40px">
                                </i>Prestation à domicile</h5>
                            <p>Toulouse, Borderouge, Balma</p>

                        </div>
                        <div>

                            <h5><i id="icon" class="fa fa-whatsapp" style="color: #017a30; font-size:40px">
                                </i>Téléphone
                            </h5>
                            <p>09.56.23.12.45</p>
                        </div>
                        <div>

                            <h5> <i id="icon" class="fa fa-at" style="color: #017a30; font-size:40px"> </i> Courriel
                            </h5>
                            <p>contact@dietform.fr</p>
                        </div>
                    </aside>
                    <aside id="form2">
                        <h4><i id="icon" class="fa fa-envelope" style="color: #017a30; font-size:40px"> </i> Contacte et
                            rendez-vous </h4>
                        <div>
                            <label for=""> Nom et Prénom</label>

                            <br><input type="text" placeholder="Mon prénom" tabindex="1" required="" data-name="Name"></span>
                        </div>
                        <div>
                            <label for="">Téléphone </label>
                            <br> <input type="number">
                        </div>
                        <div><label for=""> Email</label>
                            <br> <input type="email" id="identification" name="identification ">
                        </div>
                        <div>
                            <label for="">Message</label>
                            <br><textarea name="" id="" cols="30" rows="5"> </textarea>
                        </div>
                    </aside>
                </section>
                <section id="formbtn"><input id="submit" class="btn btn-success" type="button" value="Envoyer"></section>


            </form>

            <a href="#" class="btn " style="float: right;"><i class="bi bi-arrow-up-circle-fill" style="text-align: end; color: #017a30; font-size:60px"></i></a>
        </div>
    </main>


    <section id="sct_footer">
        <footer id="footer">

            <h2> MEILLEURE-SANTE@copyright</h2>

        </footer>
    </section>



</body>

</html>