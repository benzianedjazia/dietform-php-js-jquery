<?php

include_once '../libraries/autoloader_.php';

//Autoloader::register();

function login() {
    $identifiant = htmlspecialchars($_POST['identifiant']);
    $password = htmlspecialchars($_POST['password']);
    $PDOFactory = new PDOFactory("mysql:host=localhost;dbname=dietform_tbl", "root", "", true);
    $ManagerCompte = new ManagerAuthentification($PDOFactory->getPDO());
    if ($ManagerCompte->access($identifiant, $password)):
        $Administrateur = $ManagerCompte->getDatasSession($identifiant, $password);
        getCurrentSession($Administrateur);
        header("http://www.dietform.fr/application/views/about.php");
        Route::redirection('home2');
    else:
        Route::defaultRedirection();
    endif;
}

function getCurrentSession(Administrateur $Administrateur){
    session_start();
    $_SESSION['admin_name'] = $Administrateur->getCompleteName();
    $_SESSION['access_ctrl'] = hash("sha256",session_id()."913746");
}

function logout() {
    unset($_SESSION['admin_name']);
    unset($_SESSION['access_ctrl']);
    session_destroy();
    Route::defaultRedirection();
}

if (isset($_POST['action'])) :
    switch ($_POST['action']) :
        case "login":
            login();
            break;
        case "logout":
            logout();
            break;
        default:
            Route::defaultRedirection();
            break;
    endswitch;
else:
    Route::defaultRedirection();
endif;




    