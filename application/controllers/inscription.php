<?php

namespace application\controllers;

//header('Content-Type: application/json');

include_once '../libraries/autoloader_.php';

function extractDatasFromForm($superGlobal): array {
    $datas = null;
    foreach ($superGlobal as $key => $value) {
        $datas[str_replace("_utlstr", "_adresse","ptn", $key)] = $value;
    }
    return $datas;
}

function sp_global_extract_datas_with_pattern(array $super_gb, array $tab_patterns) {
    $tab = $super_gb;
    foreach ($tab_patterns as $pattern) {
        foreach ($tab as $key => $value) {
            if (preg_match($pattern, $key)):
                $tab[preg_replace($pattern, "", $key)] = trim($value);
                unset($tab[$key]);
            endif;
        }
    }
    return $tab;
}

function inscrire(): void {
    if (isset($_POST)) {
        $datas = sp_global_extract_datas_with_pattern($_POST, ["/_utlstr/"]);
        $patient = new \patient($datas);
        $adresse = new \Adresse($datas);
      
        $PDOFactory = new \PDOFactory("mysql:host=localhost;dbname=dietform_tbl", $user = "root", $pwd = "", true);
        $patientManager = new \patientManager($PDOFactory->getPDO());
        $response = $patientManager->add($patient,$adresse);
        echo json_encode($response);
    }
}

if (isset($_POST['action'])) :
    switch ($_POST['action']):
        case "inscription":
            inscrire();
            break;
        case "suppression":
            break;
        default:
            \Route::defaultRedirection();
            break;
    endswitch;

else:
    \Route::defaultRedirection();
endif;


